from DrissionPage._configs.chromium_options import ChromiumOptions
from DrissionPage._pages.chromium_page import ChromiumPage

from dp_pligin.hook.request_hook import SetRequestResponse

#
# def test1(tab):
#     tab.get("https://www.baidu.com")
#
#
# if __name__ == '__main__':
#     pool = TabPool(size=2)
#     for i in range(1, 10000):
#         pool.operate(test1)
#     pool.close_web_page_pool()

co = ChromiumOptions()
co.set_browser_path('C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe')
page = ChromiumPage(co)
page.add_init_js("""
// 保存原始的 setInterval 方法  
const originalSetInterval = window.setInterval;  

// 用于记录所有 setInterval 调用  
const intervals = [];  

// 重写 setInterval 方法  
window.setInterval = function(callback, delay, ...args) {  
    // 记录 setInterval 调用  
    const intervalId = originalSetInterval(function() {  
        // 这里可以插入自定义逻辑，如修改 callback 的参数  
        console.log('setInterval called with delay:', delay);  
        
        // 调用原始回调  
        //callback(...args);  
    }, delay);  

    // 保存 intervalId 以便后续管理  
    intervals.push(intervalId);  
    return intervalId; // 返回 intervalId 以保持原有功能  
};  


/*请求响应修改器1.0*/
class HttpRequest extends window.XMLHttpRequest {
    send() {
        const arr = [...arguments];
        return super.send(...arr)
    }
    open() {
        const arr = [...arguments];
        const url = arr[1];
        debugger;
        return super.open(...arr)
    }
}


window.XMLHttpRequest = HttpRequest;
""")
with open('7.aaff7e1f.js', 'r', encoding='utf-8') as file:
    response = file.read()
urls = [{'url': 'https://cpquery.cponline.cnipa.gov.cn/js/7.',
         'response': response}]
SetRequestResponse(page, urls)
page.get('https://cpquery.cponline.cnipa.gov.cn/chinesepatent/index')
