# dp_pligin

#### 介绍

dp增强插件

#### 使用说明

## tab_pool

利用多个tab，进行多线程爬取，可以控制tab数量

操作完成后必须调用pool.close_web_page_pool()，来关闭全部tab

该方法还能等待未完成的线程执行完他们的任务

<br/>

```python
from dp_pligin.tab_pool.tab_pool import TabPool


def test1(tab):
    tab.get("https://www.baidu.com")


if __name__ == '__main__':
    pool = TabPool(size=8)
    for i in range(1, 10000):
        pool.operate(test1)
    pool.close_web_page_pool()

```

## HookPage

HookPage继承了ChromiumPage

作用为给初始化的时侯的函数的做hook

例子

```python
from dp_pligin.hookj.hook_page import HookPage

if __name__ == '__main__':
    page = HookPage()
    page.hook('document', 'cookie', '')
    page.get("https://www.baidu.com")
```

这样页面打开开发人员调试的时候，如果出现页面置入cookie的情况，则进入debugger模式，方便调试观察改cookie生成的栈帧

```python
 page.hook('document', 'cookie', 'token')
```

这样标识拦截包含字符串‘token’的cookie
