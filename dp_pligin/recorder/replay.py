import json

from DrissionPage._pages.chromium_page import ChromiumPage


def replay(filename):
    with open(filename, 'r') as f:
        datas = f.read()
        datas = json.loads(datas)
        for data in datas:
            controls(data)


def controls(data):
    type_ = data['type']
    xpath = data['xpath']
    if type_ == 'click':
        page.ele(f"x:{xpath}").click()
    elif type_ == 'compositionend':
        page.ele(f"x:{xpath}").input(data['input'])


def init():
    page = ChromiumPage()

    return page


if __name__ == '__main__':
    page = init()
    page.get("https://www.baidu.com")
    replay('record_list/2024-07-18_11-05-18.json')
